<?php

use System\Support\DB;
use System\Http\Request;
use System\Http\Response;

$route->get('home', '/', function(Request $request) {
    return view('welcome');
});

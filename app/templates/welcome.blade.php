<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Under Construction</title>

    <link href="/assets/images/favicon.ico" rel="icon" type="image/x-icon">
    <link href="/assets/css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
    <header>
    <nav class="navbar navbar-dark bg-dark">
        <div class="container-fluid">
        <a class="navbar-brand" href="/">Venasoft Echo</a>
        </div>
    </nav>
    </header>
    <main class="mt-5">
    <div class="container">
        <div class="row">
        <div class="col-12">
            <h1 class="h4">Page under construction</h1>
            <p class="mt-4">Please come back again later.</p>
        </div>
        </div>
    </div>
    </main>
    <footer class="mt-5">
    <div class="container-fluid">
        <div class="row">
        <div class="col-12">
            <p class="small text-muted text-center">&copy; {{ date('Y') }} Venasoft Systems</p>
        </div>
        </div>
    </div>
    </footer>
    <script src="/assets/js/app.js" type="text/javascript"></script>
</body>
</html>